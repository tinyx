/* include/tinyx/list.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TINYX_LIST_H
#define __TINYX_LIST_H

#include <tinyx/kernel.h>

/*
 * Simple doubly linked list
 */
struct list_head {
	struct list_head	*next;
	struct list_head	*prev;
};

#define DECLARE_LIST_HEAD(head)		\
	struct list_head head = {	\
		.next = &head,		\
		.prev = &head,		\
	};

/*
 * init_list_head - initialize a doubly linked list
 *
 * @list:	the list being initialized
 */
static void inline init_list_head(struct list_head *list)
{
	list->next = list;
	list->prev = list;
}

/*
 * list_add_tail - add a new entry at the end of the list
 *
 * @head:	the head of the current list
 * @queue:	the member to insert
 */
static void inline list_add_tail(struct list_head *head, struct list_head *queue)
{
	head->next = queue->next;
	queue->next = head;
}

/*
 * list_del_tail - delete the tail from the list
 *
 * @head:	the head of the current list
 * @tail:	the tail to be removed
 */
#define list_del_tail(head, tail)	\
	(tail) = (head)->prev;		\
	(head)->next = tail;

/*
 * list_for_each - iterate over a list
 *
 * @head:	the head of the current list
 * @loop:	the struct list_head * to use as loop cursor
 */
#define list_for_each(head, loop) \
	for ((loop) = (head)->next; (loop) != (head); (loop) = (loop)->next)

/*
 * list_entry - get the struct for this entry
 *
 * @head:	the list_head pointer
 * @type:	the type of the struct this is embedded in
 * @member:	the name of the list_head within @type
 */
#define list_entry(head, type, member) \
	container_of(head, type, member);

/*
 * list_empty - tests if a list is empty
 *
 * @head:	the head of the list to be tested
 */
static inline int list_empty(const struct list_head *head)
{
	return head->next == head;
}

#endif /* __TINYX_LIST_H */
