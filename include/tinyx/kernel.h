/* include/tinyx/kernel.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TINYX_KERNEL_H
#define __TINYX_KERNEL_H

#include <asm/stddef.h>
#include <tinyx/errno.h>

typedef int (*initcall_t)(void);

#define __initcall(level, fn) \
    static initcall_t __initcall_##fn __attribute__ ((section (".initcall." level ".init"))) = fn

#define first_init(fn) __initcall("0", fn)
#define early_init(fn) __initcall("1", fn)
#define standard_init(fn) __initcall("2", fn)
#define late_init(fn) __initcall("3", fn)
#define last_init(fn) __initcall("4", fn)

#define __init __attribute__ ((__section__ (".init_functions")))

#define offsetof(t, m) ((int) &((t *)0)->m)

#define container_of(p, t, m) ({ \
	const typeof( ((t *)0)->m ) *__p = (p);	\
	(t *)( (char *)__p - offsetof(t, m) );})

#endif /* __TINYX_KERNEL_H */
