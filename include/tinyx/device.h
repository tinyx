/* include/tinyx/device.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TINYX_DEVICE_H
#define __TINYX_DEVICE_H

#include <tinyx/list.h>

struct bus_type;
struct device_driver;
struct device;

struct resource_mem {
	unsigned long	start;
	unsigned long	end;
};

struct resource_irq {
	unsigned int	start;
};

struct resource {
	union {
		struct resource_mem mem;
		struct resource_irq irq;
	};
};

struct bus_type {
	struct list_head	queue;
	unsigned int		type;
	void			*private_data;

	int (*probe) (struct bus_type *bus);
	int (*remove) (struct bus_type *bus);
	int (*match) (struct bus_type *bus,
		      struct device *dev, struct device_driver *drv);

	/* These write* and read* exists in order to avoid if's
	 * inside critial buses. */
	void (*writeb) (struct device *dev,
			unsigned char val, unsigned long addr);
	void (*writew) (struct device *dev,
			unsigned int val, unsigned long addr);
	void (*writel) (struct device *dev,
			unsigned long val, unsigned long addr);

	unsigned char (*readb) (struct device *dev, unsigned long addr);
	unsigned int (*readw) (struct device *dev, unsigned long addr);
	unsigned long (*readl) (struct device *dev, unsigned long addr);

	void (*ioctl) (struct device *dev, void *data, int type);
};

struct device_driver {
	struct list_head	queue;
	const char		*name;

	int (*probe) (struct device *dev);
	int (*remove) (struct device *dev);
};

struct device {
	struct list_head	queue;
	const char		*name;
	int			id;
	struct device_driver	*drv;
	struct bus_type		*bus;
	void			*private_data;
	struct resource		*res;
};

int register_bus_type(struct bus_type *bus);
int register_device_driver(struct device_driver *drv);
int register_device(struct device *dev, struct bus_type *bus);

#endif /* __TINYX_DEVICE_H */
