/* include/asm-generic/clock.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ASM_GENERIC_CLOCK_H
#define __ASM_GENERIC_CLOCK_H

#define CLK_ENABLED		(1 << 0)
#define CLK_INITIALIZED		(1 << 1)
#define CLK_PRIMARY		(1 << 2)

struct clock {
	unsigned int		id;
	unsigned int		rate;
	unsigned int		flags;
	struct clock		*parent;
	struct clock		**child_list;

	unsigned int (*set_rate) (struct clock *clk, unsigned int rate);
	void (*propagate) (struct clock *clk);
	void (*init) (struct clock *clk);

	unsigned int		rate_list[];
};

void clk_set_list(struct clock **list, unsigned int num);
unsigned int clk_get_rate(unsigned int id);
void clk_propagate(struct clock *clk);
unsigned int clk_set_rate(struct clock *clk, unsigned int rate);

#endif /* __ASM_GENERIC_CLOCK_H */
