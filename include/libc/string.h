/* include/libc/string.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TINYX_LIBC_STRING_H
#define __TINYX_LIBC_STRING_H

#include <asm/stddef.h>

void *memset(void *mem, int data, size_t count);
void *memcpy(void *dest, const void *src, size_t count);
char *strcat(char *str1, const char *str2);
char *strchr(const char *str, int ch);
int strcoll(const char *str1, const char *str2);
int strcmp(const char *str1, const char *str2);
char *strcpy(char *str1, const char *str2);
char *strerror(int errnum);
size_t strlen(const char *str);
char *strncat(char *str1, const char *str2, size_t count);
int strncmp(const char *str1, const char *str2, size_t count);
char *strncpy(char *str1, const char *str2, size_t count);

#endif /* __TINYX_LIBC_STRING_H */
