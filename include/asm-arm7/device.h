/* include/asm-arm7/device.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ASM_DEVICE_H
#define __ASM_DEVICE_H

/* ARM7 Buses */
#define BUS_TYPE_PLATFORM	0x00
#define BUS_TYPE_I2C		0x01
#define BUS_TYPE_SPI		0x02
#define BUS_TYPE_UART		0x03
#define BUS_TYPE_USB		0x04

#endif /* __ASM_DEVICE_H */
