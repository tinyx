/* include/asm-arm7/arch-lpc2xxx/io.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IO_H
#define __IO_H

#define __raw_iob(r)		(*((volatile unsigned char *) (r)))
#define __raw_iow(r)		(*((volatile unsigned short *) (r)))
#define __raw_iol(r)		(*((volatile unsigned long *) (r)))

#define __raw_writeb(v,a)	__raw_iob(a) = (v)
#define __raw_writew(v,a)	__raw_iow(a) = (v)
#define __raw_writel(v,a)	__raw_iol(a) = (v)

#define __raw_readb(a)		__raw_iob(a)
#define __raw_readw(a)		__raw_iow(a)
#define __raw_readl(a)		__raw_iol(a)

#endif
