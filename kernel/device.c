/* kernel/device.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tinyx/device.h>
#include <tinyx/list.h>
#include <string.h>

DECLARE_LIST_HEAD(bus_list);
DECLARE_LIST_HEAD(driver_list);
DECLARE_LIST_HEAD(device_list);

static void *match_device_driver(struct device *dev, struct device_driver *drv)
{
	struct list_head *list;
	struct list_head *entry;
	void *item;
	int ret;

	if (!dev)
		list = &device_list;
	else
		list = &driver_list;

	list_for_each(list, entry) {
		if (!dev) {
			item = list_entry(entry, struct device, queue);
			ret = strcmp(((struct device *)item)->name, drv->name);
		} else {
			item = list_entry(entry, struct device_driver, queue);
			ret = strcmp(dev->name,
					((struct device_driver *)item)->name);
		}
		if (!ret)
			break;
		else
			item = NULL;
	}

	return item;
}

int register_bus_type(struct bus_type *bus)
{
	int ret = 0;

	if (bus == NULL)
		return -ENODEV;

	if (bus->probe)
		ret = bus->probe(bus);
	if (!ret)
		list_add_tail(&bus_list, &bus->queue);

	return ret;
}

int register_device_driver(struct device_driver *drv)
{
	int ret = 0;
	struct device *dev;

	if (drv == NULL)
		return -ENODEV;

	dev = match_device_driver(NULL, drv);
	if (dev) {
		if (dev->bus->match)
			ret = dev->bus->match(dev->bus, dev, drv);
		if (!ret && drv->probe)
			ret = drv->probe(dev);
	}

	if (!ret)
		list_add_tail(&driver_list, &drv->queue);

	return ret;
}

int register_device(struct device *dev, struct bus_type *bus)
{
	int ret = 0;
	struct device_driver *drv;

	if (dev == NULL)
		return -ENODEV;

	dev->bus = bus;
	drv = match_device_driver(dev, NULL);
	if (drv) {
		if (bus->match)
			ret = bus->match(bus, dev, drv);
		if (!ret && drv->probe)
			ret = drv->probe(dev);
	}

	if (!ret)
		list_add_tail(&device_list, &dev->queue);
	else
		dev->bus = NULL;

	return ret;
}
