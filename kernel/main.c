/* kernel/main.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm/arch-lpc2xxx/lpc2xxx.h>
#include <asm/arch-lpc2xxx/start_lpc2148.h>
#include <asm/arch/clock.h>
#include <asm/io.h>
#include <tinyx/list.h>
#include <tinyx/kernel.h>

#define UART_BPS(pclk,bps) (pclk / ((bps) * 16.0))

/* definitions for some common bitrates */
#define B1200(pclk)         UART_BPS(pclk,1200)
#define B2400(pclk)         UART_BPS(pclk,2400)
#define B9600(pclk)         UART_BPS(pclk,9600)
#define B19200(pclk)        UART_BPS(pclk,19200)
#define B38400(pclk)        UART_BPS(pclk,38400)
#define B57600(pclk)        UART_BPS(pclk,57600)
#define B115200(pclk)       UART_BPS(pclk,115200)

static void init_uart0(void)
{
	/* unsigned int clk = B115200((CRYSTAL_FREQUENCY * PLL_FACTOR) / VPBDIV_FACTOR); */
	unsigned int clk = B115200(clk_get_rate(PCLK));
	__raw_writel(0x5, PINSEL0);
	__raw_writel(0x80, U0LCR);
	__raw_writel((clk & 0xff), U0DLL);
	__raw_writel((clk >> 8), U0DLM);
	__raw_writel(0x03, U0LCR);
	__raw_writel(0x00, U0FCR);
}

static void sendchar (int ch)
{
	while (!(__raw_readl(U0LSR) & 0x20));
	__raw_writel(ch, U0THR);
}

static void sendstring(const char *str)
{
	while (*str != '\0') {
		if (*str == '\n')
			sendchar('\r');
		sendchar(*str);
		str++;
	}
}

static int getkey (void)
{
	while (!(__raw_readl(U0LSR) & 0x01));
	return (__raw_readl(U0RBR));
}

extern initcall_t __initcall_start, __initcall_end;

static void do_initcalls(void)
{
	initcall_t *call_p;

	for(call_p = &__initcall_start; call_p < &__initcall_end; call_p++)
		(*call_p)();
}

DECLARE_LIST_HEAD(mylist);

struct test_list {
	char id;
	struct list_head list_queue;
};

struct test_list l1 = {
	.id = '1',
};

struct test_list l2 = {
	.id = '2',
};

extern unsigned long __heap;

int main(void)
{
	struct list_head *entry;

	init_uart0();

	sendstring(__FUNCTION__);

	do_initcalls();

	sendstring("David Cohen\n");

	list_add_tail(&mylist, &l1.list_queue);
	list_add_tail(&mylist, &l2.list_queue);

	list_for_each(&mylist, entry) {
		struct test_list *t;

		t = list_entry(entry, struct test_list, list_queue);
		sendchar(t->id);
		sendchar('\n');
	}

	return 0;
}
