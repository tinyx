KFAMILY = 0
KVERSION = 0
KREVISION = 0
KNAME = And we have a coacervate...

HOSTCC = gcc
HOSTCFLAGS = -Wall -Wstrict-prototypes -O2 -fomit-frame-pointer

CROSS_COMPILE ?= arm-linux-

ARCH = arm7
CPU = lpc2xxx

AS = $(CROSS_COMPILE)as
LD = $(CROSS_COMPILE)ld
CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar
NM = $(CROSS_COMPILE)nm
STRIP = $(CROSS_COMPILE)strip
OBJCOPY = $(CROSS_COMPILE)objcopy
OBJDUMP	= $(CROSS_COMPILE)objdump
AWK = awk
CFLAGS = -Wall -Os -mcpu=arm7tdmi -nostdlib
INCLUDES = -Iinclude -Iinclude/libc

PYTHON = python

MAKEFLAGS += --no-print-directory

all-sources = $(shell git-ls-files)

export ARCH CPU AS LD CC AR NM STRIP OBJCOPY OBJDUMP AWK CFLAGS PYTHON INCLUDES

tinyx.dirs = lib/ arch/ kernel/

tinyx.objs = $(patsubst %/, %/tyx_part.o, $(tinyx.dirs))

$(shell rm -f $(tinyx.objs) include/asm/arch include/asm)
$(shell ln -s asm-$(ARCH) include/asm)
$(shell ln -s arch-$(CPU) include/asm/arch)

_all:

all:
	@$(MAKE) _dep
	@$(MAKE) _all

_dep:
	@touch build/tinyx.deps

_all: tinyx.hex

tinyx.hex: tinyx.elf
	$(OBJCOPY) -O ihex build/$< build/$@

tinyx.elf: $(tinyx.objs)
	#$(LD) -EL -T arch/$(ARCH)/kernel/kernel.ld -o build/$@ $(tinyx.objs)
	$(CC) -Xlinker --script=arch/$(ARCH)/kernel/kernel.ld -o build/$@ $(tinyx.objs)

$(tinyx.objs):
	@$(MAKE) target=$@ -f scripts/Makefile.build

define xtags
	ctags -a $(all-sources)
endef

tags:
	$(call xtags, ctags)
.PHONY: clean
clean:
	@cleandirs="$(shell $(PYTHON) scripts/expandtree.py $(tinyx.dirs))"; \
	cleanobjs=; \
	for dir in $$cleandirs; do \
	  cleanobjs="$$cleanobjs $$dir*.o"; \
	done; \
	echo "rm -f $$cleanobjs include/asm build/tinyx.hex build/tinyx.elf build/tinyx.deps"; \
	rm -f include/asm/arch \
	rm -f $$cleanobjs include/asm build/tinyx.hex build/tinyx.elf build/tinyx.deps;


