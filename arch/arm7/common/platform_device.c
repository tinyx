/* arch/arm7/common/platform_device.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <tinyx/device.h>
#include <tinyx/kernel.h>
#include <asm/io.h>
#include <asm/device.h>
#include <asm/platform_device.h>

static void platform_writeb(struct device *dev,
			    unsigned char val, unsigned long addr)
{
	__raw_writeb(val, addr);
}

static void platform_writew(struct device *dev,
			    unsigned int val, unsigned long addr)
{
	__raw_writew(val, addr);
}

static void platform_writel(struct device *dev,
			    unsigned long val, unsigned long addr)
{
	__raw_writel(val, addr);
}

static unsigned char platform_readb(struct device *dev, unsigned long addr)
{
	return __raw_readb(addr);
}

static unsigned int platform_readw(struct device *dev, unsigned long addr)
{
	return __raw_readw(addr);
}

static unsigned long platform_readl(struct device *dev, unsigned long addr)
{
	return __raw_readl(addr);
}

static struct bus_type platform_bus = {
	.type	= BUS_TYPE_PLATFORM,
	.writeb	= platform_writeb,
	.writew	= platform_writew,
	.writel	= platform_writel,
	.readb	= platform_readb,
	.readw	= platform_readw,
	.readl	= platform_readl,
};

int register_platform_device(struct device *dev)
{
	return register_device(dev, &platform_bus);
}

static int __init init_platform(void)
{
	register_bus_type(&platform_bus);

	return 0;
}

early_init(init_platform);
