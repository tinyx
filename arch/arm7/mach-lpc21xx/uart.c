/* arch/arm7/mach-lpc21xx/uart.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm/arch-lpc2xxx/start_lpc2148.h>
#include <asm/arch-lpc2xxx/clock.h>
#include <tinyx/device.h>
#include <tinyx/kernel.h>

#define DRIVER_NAME "lpc2xxx_uart"

static int lpc2xxx_uart_probe(struct device *dev)
{
	return 0;
}

static struct device_driver lpc2xxx_uart_driver = {
	.name	= DRIVER_NAME,
	.probe	= lpc2xxx_uart_probe,
};

static int __init lpc2xxx_uart_init(void)
{
	return register_device_driver(&lpc2xxx_uart_driver);
}

early_init(lpc2xxx_uart_init);
