/* arch/arm7/mach-lpc21xx/clock.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm/io.h>
#include <asm/arch/lpc2xxx.h>
#include <asm/arch/start_lpc2148.h>

#include "clock.h"

#define VAL_FEED1	0xAA
#define VAL_FEED2	0x55

#define FCCO_MIN	156000000
#define FCCO_MAX	320000000

#define PLLCON_DIS	(0 << 0)	/* PLL Disable */
#define	PLLCON_EN	(1 << 0)	/* PLL Enable */
#define PLLCON_CON	(1 << 1)	/* PLL Connect */
#define PLLSTAT_LOCK	(1 << 10)	/* PLL Lock Status */

static unsigned int psel[4][2] = {
	{ 1, 0x0 << 5 },
	{ 2, 0x1 << 5 },
	{ 4, 0x2 << 5 },
	{ 8, 0x3 << 5 },
};

static unsigned int cclk_get_psel(unsigned int msel, unsigned int rate)
{
	unsigned int p = 4;
	unsigned int fcco = rate * 2 * msel;

	if ((fcco * psel[0][p]) < FCCO_MIN)
		/* BUG: FCCO out of range */
		goto end;

	while (p) {
		if ((fcco * psel[0][p]) > FCCO_MAX)
			p--;
		else
			goto end;
	}

	/* if ((fcco * psel[0][p]) > FCCO_MAX); BUG: FCCO out of range */

end:
	return psel[1][p];
}

static void cclk_set_pllcon(unsigned int con)
{
	__raw_writew(con, PLLCON);
	__raw_writew(VAL_FEED1, PLLFEED);
	__raw_writew(VAL_FEED2, PLLFEED);
}

static unsigned int cclk_set_rate(struct clock *clk, unsigned int rate)
{
	unsigned int msel;
	unsigned int psel;

	msel = rate / FOSC;
	msel -= msel ? 1 : 0;

	psel = cclk_get_psel(msel, rate);

	/* Disabling PLL */
	cclk_set_pllcon(PLLCON_DIS);

	/* Configuring PLL */
	__raw_writew((msel | psel), PLLCFG);
	cclk_set_pllcon(PLLCON_EN);

	/* Waiting for PLL Lock */
	while (!(__raw_readw(PLLSTAT) & PLLSTAT_LOCK));

	/* Connecting PLL */
	cclk_set_pllcon(PLLCON_EN | PLLCON_CON);

	return (msel+1) * FOSC;
}

static unsigned int pclk_set_rate(struct clock *clk, unsigned int rate)
{
	return 0;
}

static void pclk_propagate(struct clock *clk)
{
}

