/* arch/arm7/mach-lpc21xx/clock.h
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARCH_MACH_LPC2XXX_CLOCK_H
#define __ARCH_MACH_LPC2XXX_CLOCK_H

#include <asm/arch-lpc2xxx/clock.h>
#include <tinyx/kernel.h>

static unsigned int cclk_set_rate(struct clock *clk, unsigned int rate);
static unsigned int pclk_set_rate(struct clock *clk, unsigned int rate);
static void pclk_propagate(struct clock *clk);

static struct clock pclk;
static struct clock cclk;

static struct clock pclk = {
	.id		= PCLK,
	.flags		= CLK_ENABLED,
	.rate		= 12000000,
	.parent		= &cclk,
	.child_list	= NULL,
	.set_rate	= pclk_set_rate,
	.propagate	= pclk_propagate,
	.rate_list	= { 12000000, 0 },
};

static struct clock *cclk_childs[2] = { &pclk, NULL };

static struct clock cclk = {
	.id		= CCLK,
	.flags		= CLK_ENABLED | CLK_PRIMARY,
	.rate		= 12000000,
	.child_list	= cclk_childs,
	.set_rate	= cclk_set_rate,
	.propagate	= NULL,
	.rate_list	= { 12000000, 48000000, 0 },
};

struct clock *lpc2xxx_clk_list[] = { &cclk, &pclk , NULL };

#endif /* __ARCH_MACH_LPC2XXX_CLOCK_H */
