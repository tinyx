/* arch/common/clock.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm-generic/clock.h>
#include <tinyx/list.h>

static struct clock **clk_list;

unsigned int clk_get_rate(unsigned int id)
{
	struct clock **list = clk_list;
	struct clock *clk = NULL;

	while (*list) {
		clk = *list;
		if (clk->id == id)
			break;
		list++;
	}

	return clk ? clk->rate : 0;
}

void clk_propagate(struct clock *clk)
{
	struct clock **list;

	if (clk == NULL)
		return;

	if (clk->propagate)
		(*clk->propagate)(clk);

	list = clk->child_list;

	/* Revisit: fix recursive while */
	while (*list) {
		clk_propagate(*list);
		list++;
	}
}

unsigned int clk_set_rate(struct clock *clk, unsigned int clk_rate)
{
	unsigned int *rate_list;
	struct clock **list;

	if (clk == NULL)
		return 0;

	rate_list = clk->rate_list;
	while (*rate_list) {
		if (clk_rate >= (*rate_list)) {
			clk_rate = *rate_list;
			break;
		}
		rate_list++;
	}
	if (!(*rate_list))
		clk_rate = 0;

	if (clk->set_rate)
		clk_rate = (*clk->set_rate)(clk, clk_rate);

	list = clk->child_list;
	while (*list) {
		struct clock *child = *list;

		child = *list;
		if (!(child->flags & CLK_INITIALIZED)) {
			clk_set_rate(child, child->rate);
			child->flags |= CLK_INITIALIZED;
		} else
			clk_propagate(child);
		list++;
	}

	return clk_rate;
}

void clk_set_list(struct clock **list, unsigned int num)
{
	clk_list = list;

	while (*list) {
		struct clock *clk = *list;

		if ((clk->flags & CLK_PRIMARY) &&
		   !(clk->flags & CLK_INITIALIZED)) {
			if (clk->init)
				(*clk->init)(clk);
			clk_set_rate(clk, clk->rate);
			clk->flags |= CLK_INITIALIZED;
		}
		list++;
	}
}

