/* lib/libc/strerror.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <tinyx/errno.h>

#ifdef DEBUG
/**
 * sterror - Gets errnum message and returns error message
 * @errnum: Error number
 */
char *strerror(int errnum)
{
	switch (errnum) {
	case -EPERM: return "Operation not permitted\n";
	case -ENOENT: return "No such file or directory\n";
	case -ESRCH: return "No such process\n";
	case -EINTR: return "Interrupted system call\n";
	case -EIO: return "I/O error\n";
	case -ENXIO: return "No such device or address\n";
	case -E2BIG: return "Argument list too long\n";
	case -ENOEXEC: return "Exec format error\n";
	case -EBADF: return "Bad file number\n";
	case -ECHILD: return "No child processes\n";
	case -EAGAIN: return "Try again\n";
	case -ENOMEM: return "Out of memory\n";
	case -EACCES: return "Permission denied\n";
	case -EFAULT: return "Bad address\n";
	case -ENOTBLK: return "Block device required\n";
	case -EBUSY: return "Device or resource busy\n";
	case -EEXIST: return "File exists\n";
	case -EXDEV: return "Cross-device link\n";
	case -ENODEV: return "No such device\n";
	case -ENOTDIR: return "Not a directory\n";
	case -EISDIR: return "Is a directory\n";
	case -EINVAL: return "Invalid argument\n";
	case -ENFILE: return "File table overflow\n";
	case -EMFILE: return "Too many open files\n";
	case -ENOTTY: return "Not a typewriter\n";
	case -ETXTBSY: return "Text file busy\n";
	case -EFBIG: return "File too large\n";
	case -ENOSPC: return "No space left on device\n";
	case -ESPIPE: return "Illegal seek\n";
	case -EROFS: return "Read-only file system\n";
	case -EMLINK: return "Too many links\n";
	case -EPIPE: return "Broken pipe\n";
	case -EDOM: return "Math argument out of domain of func\n";
	case -ERANGE: return "Math result not representable\n";
	case -EDEADLK: return "Resource deadlock would occur\n";
	case -ENAMETOOLONG: return "File name too long\n";
	case -ENOLCK: return "No record locks available\n";
	case -ENOSYS: return "Function not implemented\n";
	case -ENOTEMPTY: return "Directory not empty\n";
	case -ELOOP: return "Too many symbolic links encountered\n";
	case -EWOULDBLOCK: return "Operation would block\n";
	case -ENOMSG: return "No message of desired type\n";
	case -EIDRM: return "Identifier removed\n";
	case -ECHRNG: return "Channel number out of range\n";
	case -EL2NSYNC: return "Level 2 not synchronized\n";
	case -EL3HLT: return "Level 3 halted\n";
	case -EL3RST: return "Level 3 reset\n";
	case -ELNRNG: return "Link number out of range\n";
	case -EUNATCH: return "Protocol driver not attached\n";
	case -ENOCSI: return "No CSI structure available\n";
	case -EL2HLT: return "Level 2 halted\n";
	case -EBADE: return "Invalid exchange\n";
	case -EBADR: return "Invalid request descriptor\n";
	case -EXFULL: return "Exchange full\n";
	case -ENOANO: return "No anode\n";
	case -EBADRQC: return "Invalid request code\n";
	case -EBADSLT: return "Invalid slot\n";
	case -EDEADLOCK: return "Bad font file format\n";
	case -EBFONT: return "Bad font file format\n";
	case -ENOSTR: return "Device not a stream\n";
	case -ENODATA: return "No data available\n";
	case -ETIME: return "Timer expired\n";
	case -ENOSR: return "Out of streams resources\n";
	case -ENONET: return "Machine is not on the network\n";
	case -ENOPKG: return "Package not installed\n";
	case -EREMOTE: return "Object is remote\n";
	case -ENOLINK: return "Link has been severed\n";
	case -EADV: return "Advertise error\n";
	case -ESRMNT: return "Srmount error\n";
	case -ECOMM: return "Communication error on send\n";
	case -EPROTO: return "Protocol error\n";
	case -EMULTIHOP: return "Multihop attempted\n";
	case -EDOTDOT: return "RFS specific error\n";
	case -EBADMSG: return "Not a data message\n";
	case -EOVERFLOW: return "Value too large for defined data type\n";
	case -ENOTUNIQ: return "Name not unique on network\n";
	case -EBADFD: return "File descriptor in bad state\n";
	case -EREMCHG: return "Remote address changed\n";
	case -ELIBACC: return "Can not access a needed shared library\n";
	case -ELIBBAD: return "Accessing a corrupted shared library\n";
	case -ELIBSCN: return ".lib section in a.out corrupted\n";
	case -ELIBMAX: return "Attempting to link in too many shared libraries\n";
	case -ELIBEXEC: return "Cannot exec a shared library directly\n";
	case -EILSEQ: return "Illegal byte sequence\n";
	case -ERESTART: return "Interrupted system call should be restarted\n";
	case -ESTRPIPE: return "Streams pipe error\n";
	case -EUSERS: return "Too many users\n";
	case -ENOTSOCK: return "Socket operation on non-socket\n";
	case -EDESTADDRREQ: return "Destination address required\n";
	case -EMSGSIZE: return "Message too long\n";
	case -EPROTOTYPE: return "Protocol wrong type for socket\n";
	case -ENOPROTOOPT: return "Protocol not available\n";
	case -EPROTONOSUPPORT: return "Protocol not supported\n";
	case -ESOCKTNOSUPPORT: return "Socket type not supported\n";
	case -EOPNOTSUPP: return "Operation not supported on transport endpoint\n";
	case -EPFNOSUPPORT: return "Protocol family not supported\n";
	case -EAFNOSUPPORT: return "Address family not supported by protocol\n";
	case -EADDRINUSE: return "Address already in use\n";
	case -EADDRNOTAVAIL: return "Cannot assign requested address\n";
	case -ENETDOWN: return "Network is down\n";
	case -ENETUNREACH: return "Network is unreachable\n";
	case -ENETRESET: return "Network dropped connection because of reset\n";
	case -ECONNABORTED: return "Software caused connection abort\n";
	case -ECONNRESET: return "Connection reset by peer\n";
	case -ENOBUFS: return "No buffer space available\n";
	case -EISCONN: return "Transport endpoint is already connected\n";
	case -ENOTCONN: return "Transport endpoint is not connected\n";
	case -ESHUTDOWN: return "Cannot send after transport endpoint shutdown\n";
	case -ETOOMANYREFS: return "Too many references: cannot splice\n";
	case -ETIMEDOUT: return "Connection timed out\n";
	case -ECONNREFUSED: return "Connection refused\n";
	case -EHOSTDOWN: return "Host is down\n";
	case -EHOSTUNREACH: return "No route to host\n";
	case -EALREADY: return "Operation already in progress\n";
	case -EINPROGRESS: return "Operation now in progress\n";
	case -ESTALE: return "Stale NFS file handle\n";
	case -EUCLEAN: return "Structure needs cleaning\n";
	case -ENOTNAM: return "Not a XENIX named type file\n";
	case -ENAVAIL: return "No XENIX semaphores available\n";
	case -EISNAM: return "Is a named type file\n";
	case -EREMOTEIO: return "Remote I/O error\n";
	case -EDQUOT: return "Quota exceeded\n";
	case -ENOMEDIUM: return "No medium found\n";
	case -EMEDIUMTYPE: return "Wrong medium type\n";
	case -ECANCELED: return "Operation Canceled\n";
	case -ENOKEY: return "Required key not available\n";
	case -EKEYEXPIRED: return "Key has expired\n";
	case -EKEYREVOKED: return "Key has been revoked\n";
	case -EKEYREJECTED: return "Key was rejected by service\n";
	case -EOWNERDEAD: return "Owner died\n";
	case -ENOTRECOVERABLE: return "State not recoverable\n";

	default: return "Unkown error\n";
	}
}
#else
inline char *strerror(int errnum) { return 0; }
#endif
