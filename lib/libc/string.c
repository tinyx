/* lib/libc/string.c
 *
 * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
 * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
 *
 * This file is part of Tinyx Nanokernel Project.
 *
 * Tinyx is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tinyx is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

/**
 * memset - Fill a memory region with data
 * @mem: Pointer to the start of the area.
 * @data: The byte to fill the area with
 * @count: The size of the area.
 */
void *memset(void *mem, int data, size_t count)
{
	char *tmp = mem;

	while (count--)
		*tmp++ = data;
	return mem;
}

/**
 * memcpy - Copy memory areas
 * @dest: Destination of copy task
 * @src: Where the data to copy from is
 * @count: The size of the area.
 */
void *memcpy(void *dest, const void *src, size_t count)
{
	char *tmp = dest;
	const char *s = src;

	while (count--)
		*tmp++ = *s++;
	return dest;
}

/**
 * strcat - Apend one string to another
 * @str1: The destination string
 * @str2: The source string
 */
char *strcat(char *str1, const char *str2)
{
	char *tmp = str1;

	while (*str1)
		str1++;
	while ((*str1++ = *str2++) != '\0')
		;
	return tmp;
}

/**
 * strchr - Find the first ocurrence of a character in a string
 * @str: The string to search into
 * @ch: The character to find
 */
char *strchr(const char *str, int ch)
{
	for (; *str != (char)ch; ++str)
		if (*str == '\0')
			return NULL;
	return (char *)str;
}

/**
 * strcoll - Compare two strings
 * @str1: First string
 * @str2: Second string
 */
int strcoll(const char *str1, const char *str2)
{
	while ((*str1) == (*str2)) {
		if (!*str1++)
			return 0;
		str2++;
	}
	while ((*str1++) && (*str2++));

	return (*str1) ? 1 : -1;
}

/**
 * strcmp - Lex comparation of two strings
 * @str1: First string
 * @str2: Second string
 */
int strcmp(const char *str1, const char *str2)
{
	signed char ret;

	while (1) {
		if ((ret = *str1 - *str2++) != 0 || !*str1++)
			break;
	}
	return ret;
}

/**
 * strcpy - Copy the contents of str2 into str1
 * @str1: The destination string
 * @str2: The source string
 */
char *strcpy(char *str1, const char *str2)
{
	char *tmp = str1;

	while ((*str1++ = *str2++) != '\0');

	return tmp;
}

/**
 * strlen - Find the length of a string
 * @str: The string to be sized
 */
size_t strlen(const char *str)
{
	const char *tmp;

	for (tmp = str; *tmp != '\0'; ++tmp);

	return tmp - str;
}

/**
 * strncat - Append count characters of str2 into str1
 * @str1: The destination string
 * @str2: The source string
 * @count: The number of characters to append
 */
char *strncat(char *str1, const char *str2, size_t count)
{
	char *tmp = str1;

	if (count) {
		while (*str1)
			str1++;
		while ((*str1++ = *str2++) != 0) {
			if (--count == 0) {
				*str1 = '\0';
				break;
			}
		}
	}
	return tmp;
}

/**
 * strncmp - Comparison of count characters
 * @str1: The destination string
 * @str2: The source string
 * @count: The number of characters
 */
int strncmp(const char *str1, const char *str2, size_t count)
{
	signed char ret = 0;

	while (count) {
		if ((ret = *str1 - *str2++) != 0 || !*str1++)
			break;
		count--;
	}
	return ret;
}

/**
 * strncpy - Copy count characters from str2 into str1
 * @str1: The destination string
 * @str2: The source string
 * @count: The number of characters
 */
char *strncpy(char *str1, const char *str2, size_t count)
{
	char *tmp = str1;

	while (count) {
		if ((*tmp = *str2) != 0)
			str2++;
		tmp++;
		count--;
	}

	return str1;
}

