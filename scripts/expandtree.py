#/* scripts/expandree.py
# *
# * Copyright (C) 2007 David Cohen <dacohen@gmail.com>
# * Copyright (C) 2007 Felipe Balbi <me@felipebalbi.com>
# *
# * This file is part of Tinyx Nanokernel Project.
# *
# * Tinyx is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * Tinyx is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with Tinyx.  If not, see <http://www.gnu.org/licenses/>.
# */

#!/usr/bin/env python

import sys
import os

tree = []

for node in sys.argv:
	if node[-3:] == '.py':
		continue
	for root, dirs, files in os.walk(node):
		if files.count('Makefile') == 0:
			dirs = []
		else:
			tree.append(root)

strout = ''

for node in tree:
	if node[-1] != '/':
		node += '/'
	strout += '%s ' % node

sys.stdout.write(strout)

